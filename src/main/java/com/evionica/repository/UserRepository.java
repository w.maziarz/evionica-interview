package com.evionica.repository;

import com.evionica.dto.User;

import java.util.List;

public interface UserRepository {

    List<User> findAll();

}
