package com.evionica.service;

import com.evionica.dto.User;

import java.util.HashMap;
import java.util.Map;
import java.util.Optional;

public class CachedUserFinder {

    private final RepositoryUserFinder repositoryUserFinder;

    private final Map<String, User> USERS_CACHE = new HashMap<>();

    public CachedUserFinder(RepositoryUserFinder repositoryUserFinder) {
        this.repositoryUserFinder = repositoryUserFinder;
    }

    public Optional<User> findUserByEmail(String email) {
        User user = USERS_CACHE.get(email);
        if(user == null) {
            user = repositoryUserFinder.findUserByEmail(email).orElse(null);
            if(user != null) {
                USERS_CACHE.put(email, user);
            }
        }

        return Optional.ofNullable(user);
    }
}
